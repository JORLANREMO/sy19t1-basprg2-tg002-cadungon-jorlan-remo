// Excercise 3 basprog 2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <cstdlib>
#include <ctime>
using namespace std;
// excercise 1
void arrayPointer(int array[10]) 
{	
	cout << "size of array: " << sizeof(array) << endl;
	 
	for (int i = 0; i < 10; i++) 
		cout << array[i] << " ";
}
int main() 
{
	srand(time(0));
	int numberArray[10];

	for (int i = 0; i < 10; ++i)
		numberArray[i] = rand() % 10 + 1;

	cout << "size of random: " << sizeof(numberArray) << endl;
	
	arrayPointer(numberArray);

}

// excercise 2 and 3
/*void arrayPointer(int array[10])
{
	cout << "size of array: " << sizeof(array) << endl;

	for (int i = 0; i < 10; i++)
		cout << array[i] << " ";
	cout << endl;
}	
int main()
{
	srand(time(0));
	int *numberArray;
	numberArray = new int[10];


	for (int i = 0; i < 10; ++i)
		numberArray[i] = rand() % 10 + 1;

	delete[] numberArray;

	cout << "size of random: " << sizeof(numberArray) << endl;

	arrayPointer(numberArray);

}*/