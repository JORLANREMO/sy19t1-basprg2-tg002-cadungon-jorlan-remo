#include<string>
using namespace std;


#pragma once
class Cloud
{
public:
	Cloud();
	Cloud(string cname, int chp, int cmp, int clvl, int cexp, int cstr, int cdex, int cvit, int cmagic, int csprt, int clck, int cattack, int cattackp, int cdefense, int cdefensep, int cmagicattack, int cmagicdef, int cmagicdefp);
	~Cloud();

	string cName(string cloud);
	void cExp(int exp); // = 5944665
	void cHp(int cHp); // 8759
	void cMp(int cMp); // 5935
	void cLvl(int clv); // 99
	void cStr(int str); // = 253
	void cDex(int dex); // = 255
	void cVit(int vit); // = 143
	void cMagic(int magic); // = 173
	void cSprt(int sprt); // = 181
	void cLck(int lck); // = 197

	void cAttack(int attack); // 255
	void cAttackP(int attackP); // 110
	void cDefense(int defense); // 149
	void cDefenseP(int defenseP); // 66
	void cMagicAttack(int magicAtk); // 173
	void cMagicDef(int magicDef); // 181
	void cMagicDefP(int magicDefP); // 3


private:
	string cname;

	int chp;
	int cmp;
	int clvl;
	int cexp; 
	int cstr;
	int cdex; 
	int cvit; 
	int cmagic; 
	int csprt; 
	int clck;

	int cattack;
	int cattackp;
	int cdefense;
	int cdefensep;
	int cmagicattack;
	int cmagicdef;
	int cmagicdefp;
};
