#pragma once
#include <string>
using namespace std;
class Skill
{
public:
	Skill();
	Skill(string name,int dmg, int mpCost );
	~Skill();

	void sName(string name);
	void sDmg(int dmg);
	void mpCost(int mpCost);

private:
	string sname;
	int sdmg;
	int smpcost;


};

