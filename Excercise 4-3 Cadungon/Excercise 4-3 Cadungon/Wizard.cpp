#include "pch.h"
#include "Wizard.h"
#include "Skill.h"
#include <iostream>
#include <string>

using namespace std;

Wizard::Wizard()
{
}

Wizard::Wizard(string name, int hp, int mp)
{
	Wizard::mName(name);
	Wizard::mHp(100);
	Wizard::mMp(100);
	wname = name;
	whp = hp;
	wmp = mp;


}


Wizard::~Wizard()
{


}

void Wizard::mName(string name)
{
	wname = name;
	cout << "Wizard: " << wname << endl;
}

void Wizard::mHp(int hp)
{
	whp = hp;
	cout << "hp: " << whp << endl;
}

void Wizard::mMp(int mp)
{
	wmp = mp;
	cout << "mp: " << wmp << endl;
}
