#pragma once
#include <string>
using namespace std;

class Wizard
{
public:
	Wizard();
	Wizard(string name,int hp, int mp );
	~Wizard();

	void mName(string name);
	void mHp(int hp);
	void mMp(int mp);


private:
	string wname;
	int whp;
	int wmp;

};

