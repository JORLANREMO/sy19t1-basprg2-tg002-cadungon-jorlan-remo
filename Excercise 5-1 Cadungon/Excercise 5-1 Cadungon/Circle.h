#pragma once
#include <string>
using namespace std;

class Circle
{
public:
	Circle(string name, int radius);
	~Circle();

	virtual void setArea(int radius);
	virtual float getArea();

private:
	string cName;
	int cRadius;
};

