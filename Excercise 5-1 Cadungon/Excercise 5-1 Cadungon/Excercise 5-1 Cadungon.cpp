// Excercise 5-1 Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include "Shape.h"
#include "Square.h"
#include "Circle.h"
#include "Rectangle.h"
#include "Triangle.h"

using namespace std;

void printShapes(const vector<Shape*>& shapes) 
{
	for (int i = 0; i < shapes.size(); i++) 
	{
		Shape*shape = shapes[i];
		cout << shape->getName() << "/tArea" <<shape->getArea() << endl;
	}
}

int main()
{	
	//squareShape.push_back(new Shape("square", 5));
	/*vector<Shape*> circleShape;
	circleShape.push_back(new Shape("circle", 5));*/
	/*vector<Shape*> rectangleShape;
	rectangleShape.push_back(new Shape("rectangle", 5));*/
	vector<Shape*> shapeVector;

	Square* squareShape = new Square("square", 5);
	squareShape->setArea(5);
	squareShape->getArea();
	shapeVector.push_back(squareShape);

	Rectangle* rectangleShape = new Rectangle("rectangle", 3, 4);
	rectangleShape->setArea(3, 4);
	rectangleShape->getArea();
	shapeVector.push_back(rectangleShape);

	Circle* circleShape = new Circle("circle", 6);
	circleShape->setArea(6);
	circleShape->getArea();
	shapeVector.push_back(circleShape);
	

	

	printShapes(shapeVector);
}
