#pragma once
#include <string>
#include "Shape.h"
using namespace std;

class Rectangle
{
public:
	Rectangle(string name, int length, int width);
	~Rectangle();

	virtual void setArea(int length, int width);
	virtual float getArea();
private:
	string rName;
	int rLength;
	int rWidth;

};

