#pragma once
#include <string>
#include "Circle.h"
#include "Rectangle.h"
#include "Square.h"
#include "Triangle.h"

using namespace std;
class Shape
{
public:
	Shape(string name, int side);
	~Shape();

	virtual string getName();
	virtual int getSide();

	virtual void setArea(int side);
	virtual float getArea();


protected:
	
	string sName;
	int sSide;
};

