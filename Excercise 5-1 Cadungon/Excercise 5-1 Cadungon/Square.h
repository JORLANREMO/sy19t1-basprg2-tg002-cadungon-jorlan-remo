#pragma once
#include <string>
#include "Shape.h"
#include "pch.h"

using namespace std;

class Square
{
public:
	Square(string name,int side);
	~Square();
	
	virtual void setArea(int side);
	virtual float getArea();

private:
	string sName;
	int sSide;
	
};

