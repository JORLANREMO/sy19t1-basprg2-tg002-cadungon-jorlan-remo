#pragma once
#include <string>
#include "Shape.h"
using namespace std;

class Triangle
{
public:
	Triangle(string name, int height, int base);
	~Triangle();

	virtual float getArea();


private:
	string tName;
	int tHeight;
	int tBase;


};

