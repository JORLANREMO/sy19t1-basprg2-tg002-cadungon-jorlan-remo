#include "pch.h"
#include "Buff.h"
#include "Haste.h"
#include "Might.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Cocentration.h"
#include <string>
#include <iostream>

using namespace std;

Buff::Buff()
{
	this->pBuffEffects.push_back(new Heal);
	this->pBuffEffects.push_back(new Might);
	this->pBuffEffects.push_back(new IronSkin);
	this->pBuffEffects.push_back(new Cocentration);
	this->pBuffEffects.push_back(new Haste);
}


Buff::~Buff()
{
}

void Buff::setHeal(int heal)
{
	int pHeal = heal;
}

int Buff::getheal()
{
	return pHeal + 10;
}

void Buff::setMight(int might)
{
	pMight = might;
}

int Buff::getMight()
{
	return 0;
}

void Buff::setIronSkin(int ironSkin)
{
	pIronSkin = ironSkin;
}

int Buff::getIronSkin()
{
	return pIronSkin + 2;
}

void Buff::setConcentration(int concentration)
{
	pConcentration = concentration;
}

int Buff::getConcentration()
{
	return pConcentration + 2;
}

void Buff::setHaste(int haste)
{
	pHaste = haste;
}

int Buff::getHaste()
{
	return pHaste + 2;
}

void Buff::gainBuffs(Player* player)
{
	pBuffEffects[1]->setBuff(player);
}
