#pragma once
#include "Player.h"
#include "Haste.h"
#include "Might.h"
#include "Heal.h"
#include "IronSkin.h"
#include "Cocentration.h"
#include <string>

using namespace std;

class Buff :
	public Player
{
public:
	Buff();
	~Buff();

	virtual void setHeal(int heal);
	virtual int getheal();

	virtual void setMight(int might);
	virtual int getMight();

	virtual void setIronSkin(int ironSkin);
	virtual int getIronSkin();

	virtual void setConcentration(int concentration);
	virtual int getConcentration();

	virtual void setHaste(int haste);
	virtual int getHaste();

	virtual void gainBuffs(Player* player);

protected:
	int pHeal;
	int pMight;
	int pIronSkin;
	int pConcentration;
	int pHaste;
	int pBuffEffects;

};

