#pragma once
#include "Buff.h"
class IronSkin :
	public Buff
{
public:
	IronSkin();
	~IronSkin();

	 void setIronSkin(int ironSkin);
	 int getIronSkin();

protected:
	int pIronSkin;
};

