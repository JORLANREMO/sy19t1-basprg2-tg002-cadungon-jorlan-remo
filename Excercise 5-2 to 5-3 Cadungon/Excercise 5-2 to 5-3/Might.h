#pragma once
#include "Buff.h"
class Might :
	public Buff
{
public:
	Might();
	~Might();


	void setMight(int might);
	int getMight();

protected:
	int pMight;
};

