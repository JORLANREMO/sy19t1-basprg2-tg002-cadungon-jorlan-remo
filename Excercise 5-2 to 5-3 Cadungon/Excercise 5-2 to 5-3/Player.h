#pragma once
#include "Buff.h"
class Player :
	public Buff
{

public:
	Player();
	~Player();

	virtual void displayStats(int heal, int might, int ironSkin, int concentration, int haste);



protected:
	int pHeal;
	int pMight;
	int pIronSkin;
	int pConcentration;
	int pHaste;

};

