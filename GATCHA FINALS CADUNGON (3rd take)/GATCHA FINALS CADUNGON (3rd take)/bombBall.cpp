#include "pch.h"
#include "bombBall.h"
#include"player.h"
#include"gatchaBall.h"
#include<iostream>
#include<string>

using namespace std;

bombBall::bombBall()
{
	this->gBPull = 1;
	this->pBomb = 25;
	this->pPull = 1;
	this->gbName = "BOMBA";
	this->pCost = 5;
}

bombBall::~bombBall()
{
}

void bombBall::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " dealing " << this->pBomb << " damage" << endl;

	unit->toMinusHP(this->pBomb);
	unit->toAddPull(this->pPull);
	unit->toMinusCrystal(this->pCost);
	unit->bombPull += this->gBPull;
}
