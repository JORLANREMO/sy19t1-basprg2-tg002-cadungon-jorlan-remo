#pragma once
#include <string>
#include <iostream>
#include "gatchaBall.h"

using namespace std;

class player;
class bombBall : public gatchaBall
{
public:
	bombBall();

	~bombBall();

	void activate(player* unit);


private:
	int pBomb;
	int pPull;
	int pCost;
	int gBPull;


};

