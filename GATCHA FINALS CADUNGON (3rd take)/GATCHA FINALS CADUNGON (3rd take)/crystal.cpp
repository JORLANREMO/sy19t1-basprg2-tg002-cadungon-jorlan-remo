#include "pch.h"
#include "crystal.h"
#include "player.h"
#include "gatchaBall.h"
#include <iostream>
#include <string>

using namespace std;

crystal::crystal()
{
	this->gBPull = 1;
	this->pCrystal = 15;
	this->pPull = 1;
	this->gbName = "CRYSTAL";
	this->pMCrystal = 5;
}


crystal::~crystal()
{
	
}

void crystal::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " adding " << this->pCrystal << endl;
	unit->toAddCrystal(this->pCrystal);
	unit->toAddPull(this->pPull);
	unit->toAddPull(this->gBPull);
}
