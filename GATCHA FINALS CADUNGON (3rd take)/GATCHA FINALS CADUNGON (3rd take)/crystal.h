#pragma once
#include <string>
#include <iostream>
#include "gatchaBall.h"

using namespace std;


class player;
class crystal : public gatchaBall
{
public:
	crystal();

	~crystal();

	void activate(player* unit);

private:
	int pCrystal;
	int pPull;
	int pMCrystal;
	int gBPull;

};

