#include "pch.h"
#include "healBall.h"
#include"player.h"
#include"gatchaBall.h"
#include<iostream>
#include<string>

using namespace std;
using namespace std;

healBall::healBall()
{
	this->gBPull = 1;
	this->pHeal = 30;
	this->pPull = 1;
	this->gbName = "HEAL";
	this->pCost = 5;

}


healBall::~healBall()
{
}

void healBall::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " healing " << this->pHeal << endl;
	unit->toAddHP(this->pHeal);
	unit->toAddPull(this->pPull);
	unit->toMinusCrystal(this->pCost);
	unit->healPull += this->gBPull;
}
