#pragma once
#include <string>
#include <iostream>
#include "gatchaBall.h"

using namespace std;

class player;
class healBall : public gatchaBall
{
public:
	healBall();

	~healBall();


	void activate(player* unit);


private:
	int pHeal;
	int pPull;
	int pCost;
	int gbPull;
};

