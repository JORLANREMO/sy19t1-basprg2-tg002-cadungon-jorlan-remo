#include "pch.h"
#include "machine.h"
#include "healBall.h"
#include "gatchaBall.h"
#include "r.h"
#include "sr.h"
#include "ssr.h"
#include "bombBall.h"
#include <string>
#include <iostream>
#include <vector>
#include "crystal.h"
#include "player.h"


using namespace std;


machine::machine()
{
	
	this->items.push_back(new ssr);
	this->items.push_back(new sr);
	this->items.push_back(new r);
	this->items.push_back(new healBall);
	this->items.push_back(new bombBall);
	this->items.push_back(new crystal);
}


machine::~machine()
{
}

void machine::setRandom(player * unit)
{
	int random = rand() % 100 + 1;


	if (random == 1) 
	{
		items[0]->activate(unit);
	}

	else if (random >= 2 && random <= 10)
	{
		items[1]->activate(unit);
	}

	else if (random >= 11 && random <= 50)
	{
		items[2]->activate(unit);
	}

	else if (random >= 51 && random <= 55)
	{
		items[3]->activate(unit);
	}

	else if (random >= 66 && random <= 75)
	{
		items[4]->activate(unit);
	}

	else if (random >= 86 && random <= 90)
	{
		items[5]->activate(unit);
	}




}

void machine::displayBall(player *award)
{
	cout << "SSR: x "<< award->sSRPull << endl;
	cout << "SR: x " << award->sRPull << endl;
	cout << "R: x " << award->rPull << endl;
	cout << "Crystal: x " << award->crystalPull << endl;
	cout << "Bomb: x " << award->bombPull<< endl;
	cout << "Heal: x " << award->healPull<<endl;
}


