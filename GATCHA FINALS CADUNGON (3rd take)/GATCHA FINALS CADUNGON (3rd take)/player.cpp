#include "pch.h"
#include "player.h"





player::player(string name, int hp, int crystal, int rp, int pull)
{
	pName = name;
	pHp = hp;
	pCrystal = crystal;
	pRP = rp;
	pPull = pull;

	bombPull = 0;
	healPull = 0;
	rPull = 0;
	sRPull = 0;
	sSRPull = 0;
	crystalPull = 0;
}

player::~player()
{
}

void player::displayStats()
{
	cout << "Name: " << this->getName() << endl;
	cout << "HP: " << this->getHP() << endl;
	cout << "Rarity Points: " << this->getRP() << endl;
	cout << "Crystals: " << this->getCrystall() << endl;
	cout << "Pulls: " << this->getPull() << endl;
}

string player::getName()
{
	return pName;
}

string player::setName()
{
	return pName;
}

int player::toAddHP(int add)
{
	pHp += add;
	return pHp;
}

int player::getHP()
{
	return pHp;
}

int player::toMinusHP(int minus)
{
	pHp -= minus;
	return pHp;
}

int player::toAddCrystal(int add)
{
	pCrystal += add;
	return pCrystal;
}

int player::getCrystall()
{
	return pCrystal;
}

int player::toMinusCrystal(int minus)
{
	pCrystal -= minus;
	return pCrystal;
}

int player::toAddRarityPoint(int add)
{
	pRP += add;
	return pRP;
}

int player::getRP()
{
	return pRP;
}

int player::toAddPull(int add)
{
	pPull += add;
	return pPull;
}

int player::getPull()
{
	return pPull;
}

bool player::isAlive()
{
	
	return this->getHP() == 0;
}

bool player::isRick()
{
	return this->getCrystall() == 0;
}

bool player::isWinning()
{
	return this->getRP() > 100;
}




