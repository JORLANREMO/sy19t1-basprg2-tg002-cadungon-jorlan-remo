#pragma once
#include <string>
#include <iostream>
using namespace std;

class gatchBall;
class machine;
class player
{
public:
	player(string name, int hp, int crystal, int rp, int pull);
	~player();
	
	//functions
	void displayStats();


	string getName();
	string setName();

	// for hp
	int toAddHP(int add);
	int getHP();
	int toMinusHP(int minus);

	// for crystal
	int toAddCrystal(int add);
	int getCrystall();
	int toMinusCrystal(int minus);

	// for rarity point
	int toAddRarityPoint(int add);
	int getRP();
	
	// for pull
	int toAddPull(int add);
	int getPull();

	//boolean
	bool isAlive();
	bool isRick();
	bool isWinning();
	
	//collate
	int rPull;
	int sRPull;
	int sSRPull;
	int healPull;
	int bombPull;
	int crystalPull;


private:
	string pName;
	int pHp;
	int pCrystal;
	int pRP;
	int pPull;



};

