#include "pch.h"
#include "r.h"
#include"player.h"
#include"gatchaBall.h"
#include<iostream>
#include<string>

using namespace std;

r::r()
{
	this->gBPull = 1;
	this->pR = 1;
	this->pPull = 1;
	this->gbName = "R";
	this->pCost = 5;
}


r::~r()
{
}

void r::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " giving " << this->pR << endl;
	unit->toAddRarityPoint(this->pR);
	unit->toAddPull(this->pPull);
	unit->toMinusCrystal(this->pCost);
	unit->rPull += this->gBPull;
}



