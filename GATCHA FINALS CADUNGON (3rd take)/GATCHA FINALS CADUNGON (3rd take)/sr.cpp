#include "pch.h"
#include "sr.h"
#include"player.h"
#include"gatchaBall.h"
#include<iostream>
#include<string>

using namespace std;

sr::sr()
{
	this->gBPull = 1;
	this->pSR = 10;
	this->pPull = 1;
	this->gbName = "SR";
	this->pCost = 5;
}


sr::~sr()
{
}

void sr::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " giving " << this->pSR << endl;
	unit->toAddRarityPoint(this->pSR);
	unit->toAddPull(this->pPull);
	unit->toMinusCrystal(this->pCost);
	unit->sRPull += this->gBPull;

}
