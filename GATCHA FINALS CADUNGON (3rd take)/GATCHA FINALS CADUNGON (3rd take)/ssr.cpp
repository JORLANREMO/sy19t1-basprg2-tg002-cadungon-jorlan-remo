#include "pch.h"
#include "ssr.h"
#include"player.h"
#include"gatchaBall.h"
#include<iostream>
#include<string>

using namespace std;

ssr::ssr()
{
	
	this->gBPull = 1;
	this->pSSR = 50;
	this->pPull = 1;
	this->gbName = "SSR";
	this->pCost = 5;

}


ssr::~ssr()
{
}

void ssr::activate(player * unit)
{
	cout << unit->getName() << " picks up " << this->gbName << " giving " << this->pSSR << endl;
	unit->toAddRarityPoint(this->pSSR);
	unit->toAddPull(this->pPull);
	unit->toMinusCrystal(this->pCost);
	unit->sSRPull += this->gBPull;
}
