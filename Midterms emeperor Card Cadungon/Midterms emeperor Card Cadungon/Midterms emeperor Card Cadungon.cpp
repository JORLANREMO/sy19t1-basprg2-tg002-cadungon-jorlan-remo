// Midterms emeperor Card Cadungon.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>
#include <vector>
using namespace std;

void getCard(vector<string>& kaiji, vector<string>& tonegawa, int round)
{
	switch (round)
	{
		// for the first 3 rounds it will always go for emepror as 1st input
	case 1: 
	case 2:
	case 3:
	case 7:
	case 8:
	case 9:
		kaiji.push_back("Emperor");
		tonegawa.push_back("Slave");
		break;
		// for the next 3 rounds it will always go for slave as 1st input
	case 4:
	case 5:
	case 6:
	case 10:
	case 11:
	case 12:
		kaiji.push_back("Slave");
		tonegawa.push_back("Emperor");
		break;
	}
	for (int i = 0; i < 4; i++) 
	{
		kaiji.push_back("Citizen");
		tonegawa.push_back("Citizen");
	}

}

int playerCash(int moneyLength, int round)
{
	int cash;

	// emperor side
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		cash = moneyLength * 100000;
	}
	//slave side
	else if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		cash = moneyLength * 500000;
	}
	return cash;
}
// print the number
void printCards(vector<string> playerDeck) 
{

	for (int i = 0; i < playerDeck.size(); i++) 
	{
		cout << "[" << i + 1 << "] " << playerDeck[i] << endl;
	}

}

void inputCards(vector<string>& playerDeck, int &input) 
{
	cout << "What will you pick??" << endl;
	printCards(playerDeck);
	cin >> input;
	input--;
}
// bot will pick randomly on the card
void botPick(vector<string>& botDeck, int& random) 
{
	random = rand() % botDeck.size()+ 1;
	random--;
}
// deletes the card that are pressed
void deAllocate(vector<string>& remove, int x) 
{
	remove.erase(remove.begin() + x);
}
// clears it
void clearCard(vector<string>& playerDeck, vector<string>& tonegawaDeck)
{
	playerDeck.clear();
	tonegawaDeck.clear();
}
void matchUp(vector<string>& kaijiDeck, vector<string>& tonegawaDeck, int playerChoice, int botChoice, int round, int cash, int distance)
{
	bool matchup = true;
	bool playing = true;

	while (playing)
	{
		inputCards(kaijiDeck, playerChoice);
		botPick(tonegawaDeck,botChoice);
		switch (round)
		{
		case 1:
		case 2:
		case 3:
		case 7:
		case 8:
		case 9:
			// player picks the emperor card and if the enemy picks a slave they automatically lose
			if (kaijiDeck[playerChoice] == "Emperor")
			{
				if (tonegawaDeck[botChoice] == "Slave")
				{
					cout << "Defeat" << endl;
					distance -= playerChoice;
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;
					_getch();
					system("cls");
				
				}

				else
				{
					cout << "Victory" << endl;
					cash += playerCash(playerChoice, round);
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;

					_getch();
					system("cls");
				}
			}

			else
			{
				if (tonegawaDeck[botChoice] == "Slave")
				{
					cout << "Victory" << endl;
					cash += playerCash(playerChoice, round);
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;
					_getch();
					system("cls");
				}

				else
				{
					cout << "Draw" << endl;
					deAllocate(kaijiDeck, playerChoice);
					deAllocate(tonegawaDeck, botChoice);
					_getch();
					system("cls");
				}

			}
			break;
		case 4:
		case 5:
		case 6:
		case 10:
		case 11:
		case 12:

			if (kaijiDeck[playerChoice] == "Slave")
			{
				if (tonegawaDeck[botChoice] == "Emperor")
				{
					cout << "Victory" << endl;
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;
					_getch();
					system("cls");
				}

				else
				{
					cout << "Defeat" << endl;
					distance -= playerChoice;
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;
					_getch();
					system("cls");
				}
			}

			else
			{
				if (tonegawaDeck[botChoice] == "Emperor")
				{
					cout << "Defeat" << endl;
					clearCard(kaijiDeck, tonegawaDeck);
					playing = false;
					_getch();
					system("cls");
				}

				else
				{
					cout << "Draw" << endl;
					deAllocate(kaijiDeck, playerChoice);
					deAllocate(tonegawaDeck, botChoice);
					_getch();
					system("cls");
				}
			}
			break;
		}
	}
}


//void miliMeter(vector<int>& x , int& input) 
//{
//	
//	while (true) 
//	{
//		cout << "How many (mm) do you want to wager (30mm)?" << endl;
//		cin >> input;
//		if (input <= 30)
//		{
//			cout << "ok let's bet" << endl;
//			break;
//		}
//		
//		else 
//		{
//			cout << "sorry invalid amount and you are going to kill yourself" << endl;
//			_getch();
//		}
//	}
//}


// round to start the game
int roundFill(int x)
{
	int roundAdder = 1;
	for (int i = 0; i < x; i++)
	{
		roundAdder = x;
	}
	cout << "Round " << roundAdder << "/12" << endl;
	return roundAdder;
}

bool betting(int length, int distanceLength) 
{
	bool check;
	if (length <= distanceLength) 
	{
		check = true;
	}

	else if (length > distanceLength) 
	{
		check = false;
	}
	return check;
}

//int kaijiHealth(int distance) 
//{ 
//	{
//		cout << "Kaiji's Eardrum(mm):" << distance << endl;
//		return distance;
//	}
//
//}



void conclusion(int cash, int round, int input, int remaining) 
{
	if (round <= 12 && cash == 20000000 && betting(input, remaining) == true) 
	{
		cout << "You won the game but 50/ 50" << endl;
	}

	else if (round == 12 && cash < 20000000 && betting(input, remaining) == false) 
	{
		cout << "You won the game" << endl;
	}

}



int main() 
{
	srand(time(NULL));
	vector<int> milimeter;
	vector<string> kaiji;
	vector<string> tonegawa;
	int distance = 30;
	int round = 1;
	int botInput = 0;
	int input;
	int cash = 0;

	for (int i = 1; i <= 12; i++) 
	{
		
		//kaijiHealth(distance);
		roundFill(i);
		cout << "cash: " << cash << endl;
		while (true)
	{
		cout << "How much do you want to bet in (mm)" << endl;
		cout << "you still have: " << distance << endl;
		cin >> input;
		
		if (input <= distance)
		{
			cout << "ok let's bet" << endl;
			break;
		}
		
		else 
		{
			cout << "sorry invalid amount and you are going to kill yourself" << endl;
			_getch();
		}
	}
		
		betting(input, distance);
		getCard(kaiji, tonegawa, round);
		_getch();
		system("cls");
		cout << "Kaiji" << endl;
		matchUp(kaiji,tonegawa,input,botInput,round, cash, distance);
		conclusion(cash, round, input, distance);
		_getch();
		system("cls");
		round++;
		
		_getch();
		
		
	}
	
	
	
}


