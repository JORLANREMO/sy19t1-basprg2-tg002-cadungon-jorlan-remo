#pragma once
#include <string>
#include "player.h"

using namespace std;
class battle
{
public:
	battle();
	~battle();

	void attack(player* user, player* target);
	void critAttack(player * user, player* target);
	void dodgeAttack(player * user, player* target);
	void nextStage(int value);
	void heal(player * user);
	

};

