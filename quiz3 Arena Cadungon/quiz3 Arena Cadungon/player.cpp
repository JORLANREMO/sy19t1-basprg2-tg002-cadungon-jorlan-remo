#include "pch.h"
#include "player.h"
#include <iostream>
#include <string>

using namespace std;

player::player()
{
}

player::player(string name, int hp, int pow, int vit, int agi, int dex)
{
	userName = name;
	pHP = hp;
	pPow = pow;
	pVit = vit;
	pAgi = agi;
	pDex = dex;
}


player::~player()
{
}

string player::getName()
{
	return userName;
}

int player::getPhp()
{
	return pHP;
}

int player::getPow()
{
	return pPow;
}

int player::getVit()
{
	return pVit;
}

int player::getAgi()
{
	return pAgi;
}

int player::getDex()
{
	return pDex;
}

void player::displayStats()
{
	cout << "name: " << this->getName() << endl;
	cout << "hp: " << this->getPhp() << endl;
	cout << "pow:" << this->getPow() << endl;
	cout << "vit:" << this->getVit() << endl;
	cout << "agi:" << this->getAgi() << endl;
	cout << "dex" << this->getDex() << endl;
}

void player::takeDamage(int value)
{
	if (value < 0) return;
	
	this->pHP -= value;
	if (pHP < 0) pHP = 0;
}

void player::takeHeal(int value)
{
	if (value > 0)return;
	this->pHP += value;
	if (pHP > 0) pHP = 0;
}

void player::waddStats(int hp, int vit)
{
}

void player::aaddStats(int agi, int dex)
{
}

void player::maddStats(int pow)
{
}

void player::takeDodge(int value)
{
}

void player::taketurn(int value)
{
}

