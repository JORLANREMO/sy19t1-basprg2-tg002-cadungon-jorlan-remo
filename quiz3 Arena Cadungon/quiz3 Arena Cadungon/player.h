#pragma once
#include <string>
#include <cstdlib>
#include "playerClass.h"


using namespace std;
class player

{
public:
	player();
	player(string name, int hp, int pow, int vit, int agi, int dex);
	~player();

	string getName();
	int getPhp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();

	void displayStats();

	void takeDamage(int value);
	void takeHeal(int value);
	void waddStats(int hp, int vit);
	void aaddStats(int agi, int dex);
	void maddStats(int pow);
	void takeDodge(int value);
	void taketurn(int value);

private:
	
	string userName;
	int pHP;
	int pPow;
	int pVit;
	int pAgi;
	int pDex;
};

